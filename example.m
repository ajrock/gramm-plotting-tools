clear
close all
addpath(genpath(pwd));
load('data.mat')
textOptions
config

fig = histBox(age,[cDat, iDat],'binEdges',binEdges,config{:});
fig = plotSigStars(fig,statNums);
