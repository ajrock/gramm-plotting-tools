function fig = histBox(plotSep, data, varargin)
%HISTBOX Plots a paired, flipped histogram with corresponding boxplots in
%   order to visualize different layers of data.
%   HISTBOX(plotSep, data) plots a histogram-boxplot graph for each unique
%   identifier in plotSep, either as a numeric or cellstr vector. Multiple
%   colors can also be attained by inputting data as either a matrix (as
%   long as there are more samples in each group than groups), or a 
%   cell vector of numeric vectors.
%   HISTBOX(plotSep, data, ...) allows the following options:
%
%   binEdges OR nbins: same as normal histogram options. However, when
%       using binEdges, if min/max edges do not include all data, then
%       histogram will fall out of sync with boxplots
%   CustomGroupIds: Group names for any potential Color-based identifiers.
%       If this is missing or contains too few entries, group names default
%       to group1, group2, etc.
%   FigurePosition: same as figure('Position',...) argument
%   Title: same as figure('Name',...) argument
%   GreyScale: Plots color-based group identifiers on a grey scale instead.
%   TextOptions: same as options for set_text_options in the gramm package.
%   binOpts: same as options for a normal histogram. May include binEdges
%   or nbins as well, if used in a batch method
%   groupMethod: accepts either 'color' or 'lightness', same as gramm
%       package.
%   legend: Makes room and plots for the legend based on color. For smaller
%       plot/larger font sizes, may clip.
%   logScale: Plots to logarithmic scale on the 'Y' axis, resets bin edges
%       to use the logarithmic min/max edges

% To do: rmOutliers function, this explanation summary, Adding star signs

opts = parseinput(varargin{:});

% Robustness
% Allowing both matrix input and cell input
switch class(data)
    case 'double'
        if size(data,2)>size(data,1)
            data = data';
        end
        % User may have incorrectly input the data, so it is a vector
        % instead of a matrix. Check.
        if length(data)/length(plotSep)==2  
            data = reshape(data,length(data)/2,2);
        end
        if isempty(opts.CustomGroupIds) 
            colorSep = repelem(compose('group%d',1:size(data,2)),size(data,1),1);
        else
            groupIDs = opts.CustomGroupIds;
            % Make robust against insufficient group names
            groupIDs = [groupIDs,compose('group%d',length(groupIDs)+1:size(data,2))];
            colorSep = repelem(groupIDs,size(data,1),1);
        end
        data = data(:);
        colorSep = colorSep(:);
    case 'cell'
        colorSep = [];
        for ii = 1:length(data)
            checkSz = size(data{ii});
            assert(min(checkSz)==1,'For cell based input, please only use vector data!')
            if checkSz(2) ~=1
                data{ii} = data{ii}';
            end
            if isempty(opts.CustomGroupIds) || ii>length(opts.CustomGroupIds)
                thisElement = cellstr(sprintf('group%d',ii));
            else
                thisElement = opts.CustomGroupIds(ii);
            end
            colorSep = [colorSep,repelem(thisElement,size(data{ii},1),[1:size(data{ii},2)])];
        end
        colorSep = colorSep(:);
        data = [data{:}];
        data = data(:);
end

% Test given plot separators, remove empty ones
switch class(plotSep)
    case 'char'
        plotSep = cellstr(plotSep);
        [~,uniquePlotSeperators] = findgroups(plotSep);
    case 'cell'
        [~,uniquePlotSeperators] = findgroups(plotSep);
    case 'double'
        plotSep = cellstr(num2str(plotSep));
        [~,uniquePlotSeperators] = findgroups(plotSep);
end

for ii = 1:length(uniquePlotSeperators)
    if sum(cellfun(@(x) strcmp(x,uniquePlotSeperators{ii}),plotSep)) == 0
        uniquePlotSeperators(uniquePlotSeperators==ii)=[];
    end
end

if opts.legend
    plotLength = 0.85;
else
    plotLength = 0.95;
end

for ii = 1:length(uniquePlotSeperators)
    thisN = (ii-1)*2+1; 
    plotInd = repmat(strcmp(plotSep,uniquePlotSeperators{ii}),length(unique(colorSep)),1);
    g(thisN) = gramm('x',data,opts.groupMethod,colorSep,'subset',plotInd);
    g(thisN).stat_bin(opts.binOpts{:});
    g(thisN).coord_flip();
    g(thisN).set_names('column','','row','','y','','x','');
    g(thisN).set_title(uniquePlotSeperators{ii});
   
    g(thisN).set_layout_options('legend',0,'title_centering','plot',...
        'Position',[0.05+plotLength/length(uniquePlotSeperators)/2*(thisN-1) 0 1/length(uniquePlotSeperators)/2 1],... 
        'margin_height',[0.1 0.02],...
        'margin_width',[0.02 0.05],...
        'redraw',false);
    
    g(thisN+1) = gramm('y', data,'x', colorSep, opts.groupMethod ,colorSep,'subset',plotInd);
    g(thisN+1).stat_boxplot('width',1,'dodge',2);
    g(thisN+1).set_names('column','','row','','y','','x','','color','');
    g(thisN+1).set_title(' ');
    
    if ii == length(uniquePlotSeperators) && opts.legend
        g(thisN+1).set_layout_options('legend',1,'title_centering','plot',...
            'Position',[0.05+plotLength/length(uniquePlotSeperators)/2*(thisN) 0 1/length(uniquePlotSeperators)/2 1],...
            'margin_height',[0.1 0.02],...
            'margin_width',[0.02 0.05],...
            'redraw',false);
    else
        g(thisN+1).set_layout_options('legend',0,'title_centering','plot',...
            'Position',[0.05+plotLength/length(uniquePlotSeperators)/2*(thisN) 0 1/length(uniquePlotSeperators)/2 1],...
            'margin_height',[0.1 0.02],...
            'margin_width',[0.02 0.05],...
            'redraw',false);
    end    
    
    if opts.logScale
        if thisN ~=1
            g(thisN).axe_property('XTickLabels',[],'XScale','log');
        else
            g(thisN).axe_property('XScale','log');
        end
        g(thisN+1).axe_property('YTick',[],'YScale','log');
    else
        if thisN ~=1
            g(thisN).axe_property('XTickLabels',[]);
        end
        g(thisN+1).axe_property('YTick',[]);
    end
        
end
fig = figure('Units','centimeters','Position' ,opts.FigurePosition,'Name',opts.Title);
if isfield(opts,'textOpts')
    g.set_text_options(opts.textOpts{:});
end
if opts.GreyScale
    g.set_color_options('chroma_range',[0 0]);
end

g.set_limit_extra([0 0.2],[0 0.2]); 
g.axe_property('TickDir','out');
g.draw();

% Fix positioning for boxplots relative to histograms
for ii = 1:length(fig.Children)
    if any(contains(fig.Children(ii).XTickLabel,[{'group'},opts.CustomGroupIds(:)']))
        fig.Children(ii).XAxis.Color='none';
        fig.Children(ii).YAxis.Color='none';
        newLim = fig.Children(ii+3).XLim;
        fig.Children(ii).YLim = newLim;
    end
end


end


function opts = parseinput(varargin)
opts = struct('CustomGroupIds',[],'FigurePosition',[1 1 15 11],...
    'Title','Untitled','GreyScale',0,'groupMethod','color','legend',0,...
    'logScale',0);
defaultBinOpts = {'geom','stairs','normalization','count'};
inputLen = length(varargin);
for ii = 1:2:inputLen
   switch varargin{ii}
       case 'CustomGroupIds'
           opts.CustomGroupIds = varargin{ii+1};
       case 'FigurePosition'
           opts.FigurePosition = varargin{ii+1};
       case 'Title'
           opts.Title = varargin{ii+1};
       case 'TextOptions'
           opts.textOpts = varargin{ii+1};
       case 'GreyScale'
           opts.GreyScale = varargin{ii+1};
           if opts.GreyScale
               opts.groupMethod = 'lightness';
           end
       case 'binOpts'
           opts.binOpts = varargin{ii+1};
       case 'binEdges'
           opts.binEdges = varargin{ii+1};
       case 'nbins'
           opts.nbins = varargin{ii+1};
       case 'legend'
           opts.legend = varargin{ii+1};
       case 'logScale'
           opts.logScale = varargin{ii+1};
           
   end
    
end

if ~isfield(opts,'binOpts')
    if isfield(opts,'binEdges')
        if opts.logScale % Transform to log scale, just in case
            opts.binEdges = logspace(log10(opts.binEdges(1)),log10(opts.binEdges(end)),length(opts.binEdges));
        end
        binOpts = [{'edges'},{opts.binEdges},defaultBinOpts(:)'];
    elseif isfield(opts,'nbins')
        binOpts = [{'nbins'},{opts.nbins},defaultBinOpts(:)'];
    else
        binOpts = {'nbins',20,'geom','stairs','normalization','count'};
    end
    opts.binOpts = binOpts;
end

end
