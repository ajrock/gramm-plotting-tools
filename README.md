# Gramm Plotting Tools

Several wrappers developed to make accessing Gramm plotting functions easier to use and more robust. Requires the Gramm package, available here.
https://github.com/piermorel/gramm


see an example usage by downloading and running example.m. From real neuroscientific experimental data.

Neuron measurements were made from one hemisphere of the brain, and acoustic sounds delivered to the ear contralateral and ipsilateral to the measurements.
These were repeated at different ages, and neuronal latency is plotted here.